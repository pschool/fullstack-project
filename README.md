# HUGO E-Commerce
[![Netlify Status](https://api.netlify.com/api/v1/badges/143eff50-aaff-4906-935d-46a956335ac7/deploy-status)](https://app.netlify.com/sites/hugofullstack/deploys)
## Description

> Static site generators with HUGO and Netlify CMS

## Used :

* Go (use HUGO)
* Netlify cms
* CSS With Bulma

## [Demo](https://hugofullstack.netlify.app/)



## Author

[@Steodec](https://paul-tedesco.com)